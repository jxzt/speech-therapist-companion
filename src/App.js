import React from "react";
import DateInputModule from "./components/DateInputModule";

class App extends React.Component {
  render() {
    return (
      <div className="container">
        <Header />
        <Body />
      </div>
    );
  }
}

class Header extends React.Component {
  render() {
    return (
      <div className="py-5 text-center">
        <h1>Speech Therapist Companion</h1>
        <p>Input date of birth below to find out their age in years; months;</p>
      </div>
    );
  }
}

class Body extends React.Component {
  render() {
    return <DateInputModule />;
  }
}

export default App;
