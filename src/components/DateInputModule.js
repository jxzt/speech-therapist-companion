import React from "react";

class DateInput extends React.Component {
  constructor(props) {
    super(props);
    const patient = {
      name: "",
      dayOfBirth: "",
      monthOfBirth: "",
      yearOfBirth: ""
    };
    const monthInfo = [
      {
        id: 1,
        month: "January",
        days: 31
      },
      {
        id: 2,
        month: "February",
        days: 28
      },
      {
        id: 3,
        month: "March",
        days: 31
      },
      {
        id: 4,
        month: "April",
        days: 30
      },
      {
        id: 5,
        month: "May",
        days: 31
      },
      {
        id: 6,
        month: "June",
        days: 30
      },
      {
        id: 7,
        month: "July",
        days: 31
      },
      {
        id: 8,
        month: "August",
        days: 31
      },
      {
        id: 9,
        month: "September",
        days: 30
      },
      {
        id: 10,
        month: "October",
        days: 31
      },
      {
        id: 11,
        month: "November",
        days: 30
      },
      {
        id: 12,
        month: "December",
        days: 31
      }
    ];

    this.state = {
      result: null,
      historyElements: [],
      clickCount: 0,
      patient: patient,
      monthInfo: monthInfo
    };
  }

  handleClearClick = e => {
    this.setState({
      result: null,
      historyElements: [],
      clickCount: 0
    });
  };

  handleSaveClick = e => {
    let userDayInput = document.getElementById("day-select").value;
    let userMonthInput = document.getElementById("month-select").value;
    let userYearInput = document.getElementById("year-select").value;
    let userPatientInput = document.getElementById("patient-name-input");

    if (userPatientInput.value !== "") {
      const dateObj = new Date();
      const currentDay = dateObj.getDate();
      const currentYear = dateObj.getFullYear();
      const currentMonth = dateObj.getMonth() + 1;

      let yearsOld = (currentYear - userYearInput) * 12;
      let monthsOld;
      let daysRemainingOld;
      //if years old = 0 then they were born in current year
      if (yearsOld === 0) {
        monthsOld = currentMonth - userMonthInput;
      } else {
        monthsOld = yearsOld - userMonthInput + currentMonth;
      }

      //calculate days remaining
      let daysInMonth = this.state.monthInfo[userMonthInput - 1].days;
      if (userDayInput < currentDay) {
        daysRemainingOld = currentDay - userDayInput;
      } else {
        daysRemainingOld = daysInMonth - userDayInput + currentDay;
      }

      const finalAge =
        "Patient : " +
        userPatientInput.value +
        " age is " +
        Math.floor(monthsOld / 12) +
        "y " +
        (monthsOld % 12) +
        "m " +
        daysRemainingOld +
        "d";
      this.setState({
        result: finalAge,
        clickCount: this.state.clickCount + 1
      });
      this.addToHistory(finalAge);
      userPatientInput.value = "";
    } else {
      this.setState({ result: "Patient name is missing" });
    }
  };

  addToHistory(result) {
    this.state.historyElements.push(
      <li key={this.state.clickCount}>{result}</li>
    );
  }

  render() {
    const monthOptionElements = [];
    for (let i = 0; i < this.state.monthInfo.length; i++) {
      monthOptionElements.push(
        <option
          key={this.state.monthInfo[i].month}
          value={this.state.monthInfo[i].id}
          id={this.state.monthInfo[i].month}
        >
          {this.state.monthInfo[i].month}
        </option>
      );
    }
    const dayOptionElements = [];
    for (let i = 1; i <= 31; i++) {
      dayOptionElements.push(
        <option key={i} value={i}>
          {i}
        </option>
      );
    }

    const MAX_AGE = 20;
    const yearArray = [];
    const dateObj = new Date();
    const currentYear = dateObj.getFullYear();
    for (let i = 0; i < MAX_AGE; i++) {
      yearArray.push(
        <option key={MAX_AGE - i} value={String(currentYear) - i}>
          {currentYear - i}
        </option>
      );
    }

    return (
      <div className="text-center">
        <select id="day-select" className="dropdown-css">
          {dayOptionElements}
        </select>
        <select id="month-select" className="dropdown-css">
          {monthOptionElements}
        </select>
        <select id="year-select" className="dropdown-css">
          {yearArray}
        </select>
        <br />
        <br />
        <div className="input-group mb-3">
          <div className="input-group-prepend">
            <span className="input-group-text" id="inputGroup-sizing-default">
              Patient Name
            </span>
          </div>
          <input
            type="text"
            id="patient-name-input"
            className="form-control"
            aria-label="Sizing example input"
            aria-describedby="inputGroup-sizing-default"
          />
        </div>
        <br />
        <button
          id="save-button"
          className="btn btn-primary"
          onClick={this.handleSaveClick}
        >
          Save
        </button>
        <button
          id="clear-button"
          className="btn btn-primary"
          onClick={this.handleClearClick}
        >
          Clear
        </button>
        <br />
        <br />

        <h5>{this.state.result}</h5>

        <h4>History</h4>
        <ul id="historyDiv">{this.state.historyElements}</ul>
      </div>
    );
  }
}

export default DateInput;
